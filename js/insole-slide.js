$(document).ready(function()
{
  //INSOLE PAGE
  if ($('body').hasClass('insole_page')) {
    //LEFT CLICK
    $(' .slide').on('click','img', function()
    {
      var currentIndex = parseInt($('.slide.central').find('img.active').attr('data-index'));
      $('.slide').find('img.active').removeClass('active');

      var centerIndex = (currentIndex == 2) ? 0 : (currentIndex + 1);
      var leftIndex = (centerIndex == 2) ? 0 : centerIndex + 1;
      var rightIndex = (centerIndex == 0) ? 2 : centerIndex - 1;
      if($(this).parents('.slide').attr('id') == 'left_slide')
      {
        centerIndex = (currentIndex == 0) ? 2 : (currentIndex - 1);
        leftIndex = (centerIndex == 2) ? 0 : centerIndex + 1;
        rightIndex = (centerIndex == 0) ? 2 : centerIndex - 1;
      }
      $('.slide.central').find('img:eq(' + centerIndex + ')').addClass('active');
      $('.slide#left_slide').find('img:eq(' + leftIndex + ')').addClass('active');
      $('.slide#right_slide').find('img:eq(' + rightIndex + ')').addClass('active');
      // SLOGAN
      $('.insole_page .slogan p.active').removeClass('active');
      $('.insole_page .slogan p:eq(' + centerIndex +')').addClass('active');
      $('.popups').each(function(index, el)
      {
        $(this).removeClass('active');
      });

      switch(centerIndex)
      {
        case 0:
          $('.popups.first, .popups.second, .popups.third').addClass('active');
          break;
        case 1:
          $('.popups.fourth, .popups.fifth, .popups.sixth').addClass('active');
          break;
        case 2:
          $('.popups.seventh, .popups.eighth, .popups.nineth').addClass('active');
          break;
        default:
          break;
      }
    });



    // $('.slidee ').on('click','img',function(){
    //   var current_slide = $('.sky-carousel-wrapper ').attr('class').split('sky-carousel-wrapper ')[1];
    //   if ($(this).parent().parent().hasClass('centered')) {
    //     return false;
    //   }else if($(this).parent().parent().hasClass('central')){
    //     return false;
    //   }
    //   else{
    //     var name_of_slide = $(this).parent().parent().attr('id');
    //     if (name_of_slide === 'left_slide') {
    //       if (current_slide === 'first') {
    //         $('.sky-carousel-wrapper ').removeClass(current_slide);
    //         $('.sky-carousel-wrapper ').addClass('third');
    //         return false;
    //       }else if(current_slide === 'second'){
    //         $('.sky-carousel-wrapper ').removeClass(current_slide);
    //         $('.sky-carousel-wrapper ').addClass('first');
    //         return false;
    //
    //       }else if(current_slide === 'third'){
    //         $('.sky-carousel-wrapper').removeClass(current_slide);
    //         $('.sky-carousel-wrapper').addClass('second');
    //         return false;
    //       }
    //     }else{
    //       if (current_slide === 'first') {
    //         $('.sky-carousel-wrapper ').removeClass(current_slide);
    //         $('.sky-carousel-wrapper ').addClass('second');
    //         return false;
    //       }else if(current_slide === 'second'){
    //         $('.sky-carousel-wrapper ').removeClass(current_slide);
    //         $('.sky-carousel-wrapper ').addClass('third');
    //         return false;
    //
    //       }else if(current_slide === 'third'){
    //         $('.sky-carousel-wrapper').removeClass(current_slide);
    //         $('.sky-carousel-wrapper').addClass('first');
    //         return false;
    //       }
    //     }
    //   }
    // });
    $('.men_woman').on('click','.fa',function(){
      if ($(this).parent().hasClass('selected')) {
        return 'male';
      }else{
        $('.men_woman').find('.selected').removeClass('selected');
        $(this).parent().addClass('selected');
        if ($(this).hasClass('fa-male')) {
          return 'male';
        }else{
          return 'female';
        }
      }
    });
    $('.wristle').children('.icon:not(:eq(0))').hide();
  }

});
