var config = {
  shopURL: 'https://avia-ascend.myshopify.com'
};

var App = {
  addToCart: function(id) {
    console.log("adding to cart: " + id);
    var f = document.createElement('form');
    f.style.display = 'none';
    document.body.appendChild(f);
    f.method = 'POST';
    f.action = config.shopURL + '/cart/add';
    // Product id
    var v = document.createElement('input');
    v.setAttribute('type', 'hidden');
    v.setAttribute('name', 'id');
    v.setAttribute('value', id);
    f.appendChild(v);
    // Return action
    var r = document.createElement('input');
    r.setAttribute('type', 'hidden');
    r.setAttribute('name', 'return_to');
    r.setAttribute('value', 'back');
    f.appendChild(r);
    f.submit();
    return false;
  },
  checkoutCart: function() {
    window.open(config.shopURL + '/checkout', 'checkoutWindow');
  },
  checkoutVariant: function(id, quantity) {
    // Format: <shop url>/cart/<variant id>:<quantity>
    var q = 1;
    if("undefined" !== typeof quantity) {
      q = quantity;
    }
    var url = config.shopURL + '/cart/' + id + ':' + q;
    window.open(url, 'checkoutWindow');
    return false;
  }
};
