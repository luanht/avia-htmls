$(document).ready(function() {
  function selectSlide(index) {
    $('.slidee .slide').each(function(i) {
      var $this = $(this);
      if(i === index) {
        $this.addClass('selected');
        var colors = [];
        var firstColor = '';

        $this.find('img[data-color]').each(function(i) {
          if(i === 0) {
            firstColor = $(this).data('color');
          }
          colors.push($(this).data('color'));
        });

        $('#color-switcher li').each(function() {
          var $_this = $(this);
          $_this.removeClass('active');
          var switchColor = $_this.data('color');
          if(colors.indexOf(switchColor) > -1) {
            $_this.removeClass('hide');
          } else {
            $_this.addClass('hide');
          }
        });
        $('#color-switcher').removeClass('hide');
      }

      $('#color-switcher li[data-color="' + firstColor + '"]').trigger('click'); // active the first color
    });
  }

  $('.slidee').on('click', '.slide', function() {
    var slideIndex = $('.slidee .slide').index(this);
  
    $(this).addClass('selected');

    if ($(this).hasClass('central')) {
      return false;
    }

    $('.price_wrp .btn').html('BUY NOW $' + $(this).attr('data-price'));
    $('#buy_btn').html('BUY NOW $' + $(this).attr('data-price'));

    $(this).parent().children('.slide').removeClass('selected');
    var img_url = $(this).find('img').attr('src');

    $currentColor = 'color-' + $(this).find('img').attr('data-color');
    $('.nav-color').find('.' + $currentColor).addClass('active');
    $(this).parent().find('.central').find('img').animate({
      opacity:0
    }, 300, function() {
      $(this).attr('src', img_url);
      $(this).animate({
        opacity: 1
      }, 300);
    }).dequeue();

    selectSlide(slideIndex);
  });

  $('.nav-color li').on('click', function() {
    if($('.slidee').find('.selected').length === 0){
      return false;
    }
    var color = $(this).attr('data-color');
    var color_url = $('.selected').find('img[data-color="' + color + '"]').attr('src');
    var $img = $('.selected').find('img[data-color="' + color + '"]');
    var variantID = $img.attr('data-product-id');
    $('#buy_btn').data('variant-id', variantID);
    $('#btn-buy-now-top').data('variant-id', variantID);
    $('.nav-color li').removeClass('active');
    $(this).addClass('active');
    $('.central').find('img').animate({
      opacity:0
    }, 300, function(){
      $(this).attr('src', color_url);
      $(this).animate({
        opacity: 1
      }, 300);
    }).dequeue();
  });  

  $('.slidee .slide:first').trigger('click');
});