$(document).ready(function(e) {
  $(document).on('click', '.btn-buy-now', function(e) {
    e.preventDefault();
    var variantID = $(this).data('variant-id');
    App.checkoutVariant(variantID);
  });

  $('#form-subscribe').on('submit', function(e) {
    e.preventDefault();
    var $input = $(this).find('input');
    $.ajax({
      url: "/subscribe/register",
      type: 'POST',
      dataType: 'JSON',
      data: {email: $input.val()},
      success: function(res) {
        if(res.success) {
          $input.val('');
        }        
        alert(res.message);
      },
      error: function() {
        alert('An error has occurred, please try again.');
      }
    });
  });
});