use Rack::Static,
  urls: ["/images", "/js", "/css", "/fonts"],
  root: ".",
  index: "landing.html"

%w(landing insole tracker checkout review).each do |page|
  map "/#{page}.html" do
    run lambda { |env|
      [
        200,
        {
          'Content-Type'  => 'text/html', 
          'Cache-Control' => 'public, max-age=86400' 
        },
        File.open("#{page}.html", File::RDONLY)
      ]
    }
  end
end